// SPDX-License-Identifier: GPL-2.0
/*
 * (C) 2021 Collabora ltd.
 */

/* Linux */
#include <linux/types.h>
#include <linux/input.h>
#include <linux/hidraw.h>
#include <time.h>

#ifndef HIDIOCSFEATURE
#warning Please have your distro update the userspace kernel headers
#define HIDIOCSFEATURE(len)    _IOC(_IOC_WRITE|_IOC_READ, 'H', 0x06, len)
#define HIDIOCGFEATURE(len)    _IOC(_IOC_WRITE|_IOC_READ, 'H', 0x07, len)
#endif
#define HIDIOCGINPUT(len)    _IOC(_IOC_WRITE|_IOC_READ, 'H', 0x0A, len)
#define HIDIOCSOUTPUT(len)    _IOC(_IOC_WRITE|_IOC_READ, 'H', 0x0B, len)
#define HIDIOCGOUTPUT(len)    _IOC(_IOC_WRITE|_IOC_READ, 'H', 0x0C, len)
#define HIDIOCSINPUT(len)    _IOC(_IOC_WRITE|_IOC_READ, 'H', 0x09, len)

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#define WRITE_REQ_LEN 63
#define READ_REQ_LEN 6
#define READ_LEN 66
#define TAIL_LEN 1
#define DATA_PACKET_HEADER 1
#define CMD_HEADER_LEN 0x5
#define DATA_PACKET_LEN (WRITE_REQ_LEN -		\
			 (CMD_HEADER_LEN + DATA_PACKET_HEADER + TAIL_LEN ))

#define PAYLOAD_SIZE (DATA_PACKET_HEADER+DATA_PACKET_LEN)

#define FT_CHIP_ID 0x542C

unsigned char *fw_map;
size_t fw_size;
int fd;
int verbose;

#define VERBOSE_PRINT(lvl, fmt, ...)				\
	do {							\
		if (verbose >= lvl)				\
			fprintf(stderr, fmt, ##__VA_ARGS__);	\
	} while (0)

#define DEBUG(fmt, ...) VERBOSE_PRINT(2, fmt, ##__VA_ARGS__)
#define LOG(fmt, ...) VERBOSE_PRINT(1, fmt, ##__VA_ARGS__)
#define INFO(fmt, ...) VERBOSE_PRINT(0, fmt, ##__VA_ARGS__)

#define MIN(x, y) ((x)<(y))? (x):(y)


static unsigned char checksum(unsigned char *data, size_t len)
{
	unsigned int i=0;
	unsigned char checksum=0;

	for(i = 0; i < len; i++)
		checksum ^= data[i];

	checksum++;
	return checksum;
}

static unsigned int checksum32(unsigned char *data, size_t len)
{
	unsigned int checksum = 0;
	size_t i;

	for(i = 0; i < len ; i += 4) {
		checksum ^= ((data[i + 3] << 24) +
			     (data[i + 2] << 16) +
			     (data[i + 1] << 8) +
			     data[i]);
	}

	checksum += 1;

	return checksum;
}

static int submit_request(unsigned char *buf, int len)
{
	int i, ret;

	DEBUG("\n[WRITE (%d)] ", len);
	for (i = 0; i < len; i++)
		DEBUG("%.2hhx ", buf[i]);
	DEBUG("\n");

	ret = write(fd, buf, len);
	if (ret < 0)
		return ret;
	return 0;
}

static int send_write_request(unsigned char cmd, unsigned char* data, int data_len)
{
	unsigned char buf[WRITE_REQ_LEN + 1];
	unsigned char *message = &buf[1];
	unsigned char len = CMD_HEADER_LEN + data_len;
	int i;

	/* First byte is not part of the message and must be zero. */
	buf[0] = 6;

	message[0] = 0xff;
	message[1] = 0xff;
	message[2] = len;
	message[3] = cmd;

	memcpy(&message[4], data, data_len);
	i = 4 + data_len;
	message[i++] = checksum(message, len - 1);

	for (; i < (WRITE_REQ_LEN + 1) ; i++)
		message[i] = 0xff;

	return submit_request(buf, WRITE_REQ_LEN + 1);
}


static int read_response(unsigned char *buf, size_t len)
{
	int ret;
	int retries = 5;

	memset(buf, '\0', len);

	do {
		usleep (20000);
		ret = read(fd, buf, len);
	} while (ret < 0 && retries--);

	DEBUG("[RESPONSE (%d)]", ret);
	if (ret > 0)
		for (int i = 0; i < ret; i++)
			DEBUG(" %hhx", buf[i]);
	else
		DEBUG(" [FAILED]");

	DEBUG("\n");

	return ret;
}

#define CMD_ENTER_UPGRADE_MODE	0x40
#define CMD_CHECK_CURRENT_STATE	0x41
#define CMD_READY_FOR_UPGRADE	0x42
#define CMD_SEND_DATA		0x43
#define CMD_UPGRADE_CHECKSUM	0x44
#define CMD_EXIT_UPGRADE_MODE	0x45
#define CMD_USB_READ_UPGRADE_ID	0x46
#define CMD_ERASE_FLASH		0x47
#define CMD_READ_REGISTER	0x50

#define VERSION_REGISTER	0xA6

#define WRITE_DATA_REQ(cmd, data, len) do { 				\
	if (send_write_request(cmd, data, len))				\
		perror("Failed to send command to device.");		\
	} while(0)

#define WRITE_REQ(cmd) WRITE_DATA_REQ(cmd, NULL, 0)

void usage()
{
	fprintf(stderr, "Usage: -d <device> [-f] <file>\n");

	exit(EXIT_FAILURE);
}

#define RESPONSE_SIZE 64

int parse_accepted_command_reply(unsigned char *buf, unsigned int len)
{
	if (len != RESPONSE_SIZE)
		return -EINVAL;

	if (buf[3] == 0x5 && buf[4] == 0xf0 && buf[5] == 0xf6)
		return 0;

	return 1;


}

int change_device_mode(int enter)
{
	int ret = -1;
	unsigned char buf[RESPONSE_SIZE];
	unsigned char cmd = enter ? CMD_ENTER_UPGRADE_MODE : CMD_EXIT_UPGRADE_MODE;

	WRITE_REQ(cmd);

	memset(buf, '\0', RESPONSE_SIZE);

	ret = read_response(buf, RESPONSE_SIZE);

	if (ret > 0 && !parse_accepted_command_reply(buf, RESPONSE_SIZE)) {
		DEBUG("Accepted\n");
		ret = 0;
	} else {
		DEBUG("Failed\n");
	}


	return ret;
}

int device_in_upgrade_mode()
{
	unsigned char buf[RESPONSE_SIZE];

	WRITE_REQ(CMD_CHECK_CURRENT_STATE);
	read_response(buf, RESPONSE_SIZE);

	if (buf[3] == 0x06 && buf[4] == 0x41 && buf[5] == 0x1 && buf[6] == 0x47) {
		DEBUG("Device is in upgrade mode\n");
		return 1;
	}

	return 0;
}

int enter_upgrade_mode()
{
	int retries = 15;
	int i;

	for (i = 0; i < retries; i++) {
		LOG("Entering upgrade mode [%d/%d]... ", (i+1), retries);

		change_device_mode(1);

		sleep (1);

		if (device_in_upgrade_mode()) {
			LOG("Success.\n");
			return 0;
		}

		LOG("failed.\n");

	}

	INFO("Couldn't enter Upgrade Mode.\n");

	return -EIO;
}

int exit_upgrade_mode()
{
	int retries = 15;
	int i;

	for (i = 0; i < retries; i++) {
		LOG("Exiting upgrade mode [%d/%d]... ", (i+1), retries);

		change_device_mode(0);

		sleep (1);

		if (!device_in_upgrade_mode()) {
			LOG("Success.\n");
			return 0;
		}

		LOG("failed.\n");
	}

	INFO("Couldn't exit Upgrade Mode.\n");

	return -EIO;
}

int validate_chip_id()
{
	unsigned char buf[RESPONSE_SIZE];
	unsigned int id;

	LOG("Reading chip ID.\n");

	WRITE_REQ(CMD_USB_READ_UPGRADE_ID);
	read_response(buf, RESPONSE_SIZE);

	id = buf[5] << 8 | buf[6];

	if (id != FT_CHIP_ID) {
		LOG("Unexpected Chip ID %hx", id);
		return -EIO;
	}
	LOG("Good chip ID.\n");
	return 0;
}

int check_ready_for_upgrade()
{
	unsigned char buf[RESPONSE_SIZE];

	LOG("Sending READY_FOR_UPGRADE.\n");

	WRITE_REQ(CMD_READY_FOR_UPGRADE);
	read_response(buf, RESPONSE_SIZE);

	if (buf[3] == 0x06 && buf[4] == 0x42
	    && buf[5] == 0x02 && buf[6] == 0x47) {
		LOG("Device is ready for upgrade.\n");
		return 0;
	}
	return -EIO;
}
#define CMD_SET_FLASH_LEN	0xB0

static unsigned int set_flash_length()
{
	int ret;
	unsigned char buf[RESPONSE_SIZE];
	unsigned char flash_len[4] = {00, 01, 00, 00};

	LOG("Sending SET_FLASH_LEN.\n");


	WRITE_DATA_REQ(CMD_SET_FLASH_LEN, flash_len, 4);

	ret = read_response(buf, RESPONSE_SIZE);

	return -1;

}

static unsigned int read_register(unsigned char reg)
{
	int ret;
	unsigned char buf[RESPONSE_SIZE];

	WRITE_DATA_REQ(CMD_READ_REGISTER, &reg, 1);

	ret = read_response(buf, RESPONSE_SIZE);

	if (ret && buf[3] == 0x7 && buf[4] == CMD_READ_REGISTER &&
	    buf[5] == reg)
		return buf[6];
	return -1;

}

unsigned int get_device_checksum(unsigned int *checksum)
{
	unsigned char buf[RESPONSE_SIZE];

	LOG("Sending UPGRADE_CHECKSUM.\n");

	WRITE_REQ(CMD_UPGRADE_CHECKSUM);
	read_response(buf, RESPONSE_SIZE);

	if (buf[3] == 0x09 && buf[4] == 0x44) {
		*checksum = *(unsigned int*)&buf[5];
		LOG("checksum is: %x\n", *checksum);
		return 0;
	}
	return -EIO;
}

unsigned int erase_flash()
{
	unsigned char buf[RESPONSE_SIZE];
	int ret;

	LOG("Sending CMD_ERASE_FLASH.\n");

	WRITE_REQ(CMD_ERASE_FLASH);

	ret = read_response(buf, RESPONSE_SIZE);

	if (ret > 0 && !parse_accepted_command_reply(buf, RESPONSE_SIZE))
		return 0;

	return -EIO;
}

#define STATE_FIRST_PACKET 0
#define STATE_MIDDLE_PACKET 1
#define STATE_LAST_PACKET 2

unsigned int write_firmware(const unsigned char *fw,
			    unsigned long fw_len)
{
	unsigned char payload[PAYLOAD_SIZE];
	unsigned char *data_packet = &payload[1];
	unsigned char buf[RESPONSE_SIZE];
	unsigned long remaining = fw_len;
	unsigned int state = STATE_FIRST_PACKET;
	unsigned long tx_len = 0;
	unsigned long offset = 0;
	int ret;

	LOG("writing firmware\n");

	while (remaining) {
		if (remaining <= DATA_PACKET_LEN)
			state = STATE_LAST_PACKET;

		tx_len = MIN(remaining, DATA_PACKET_LEN);

		payload[0] = state;
		memcpy(data_packet, &fw[offset], tx_len);

		LOG("sending off=%lu remain=%lu tx_len=%lu\n", offset,
		      remaining, tx_len);

		remaining -= tx_len;
		offset += tx_len;

		WRITE_DATA_REQ(CMD_SEND_DATA, payload, tx_len+1);

		ret = read_response(buf, RESPONSE_SIZE);
		if (ret <= 0 || parse_accepted_command_reply(buf, RESPONSE_SIZE))
			return -EIO;

		usleep(2000);

		state = STATE_MIDDLE_PACKET;
	}

	LOG("Completed firmware write");

	usleep(8000);

	return 0;
}

int main(int argc, char **argv)
{
	char *device, *fw_path;
	struct stat sb;
	int opt;
	int fw_fd;
	char go;
	int ret = 1;
	int rescue = 0, emerg = 0;
	int interactive_mode = 1;
	int version_check = 0;

	unsigned int fw_checksum;
	unsigned int current_fw_version;
	unsigned int new_device_checksum;

	while ((opt = getopt(argc, argv, "f:d:vreyqc")) != -1) {
		switch (opt) {
		case 'd':
			device = strdup(optarg);
			if (!device)
				perror("Can't allocate memory.");
			break;
		case 'f':
			fw_path = strdup(optarg);
			if (!fw_path)
				perror("Can't allocate memory.");
			break;
		case 'v':
			verbose++;
			break;
		case 'r':
			rescue = 1;
			break;
		case 'e':
			emerg = 1;
			break;
		case 'y':
			interactive_mode = 0;
			break;
		case 'q':
			verbose = -1;
			break;
		case 'c':
			version_check = 1;
			break;
		default:
			usage();
		}
	}

	if (!device)
		usage();

	fd = open(device, O_RDWR|O_NONBLOCK);
	if (fd < 0) {
		fprintf(stderr, "Unable to open %s: %s", device, strerror(errno));
		return 1;
	}

	if (!version_check) {
		if (!fw_path)
			usage();

		fw_fd = open(fw_path, O_RDONLY);
		if (fw_fd < 0) {
			fprintf(stderr, "Unable to open %s: %s\n", device,
				strerror(errno));
			return 1;
		}

		if (rescue)
			return !!exit_upgrade_mode();

		if (fstat(fw_fd, &sb) == -1) {
			fprintf(stderr, "Unable to stat %s: %s\n", fw_path,
				strerror(errno));
			return 1;
		}
		fw_size = sb.st_size;

		printf("fw_size mod = %lu\n", fw_size % 4);

		fw_map = mmap(NULL, fw_size, PROT_READ, MAP_PRIVATE, fw_fd, 0);
		if (fw_map == MAP_FAILED) {
			fprintf(stderr, "Unable to map %s: %s\n", fw_path,
				strerror(errno));
			return 1;
		}

		fw_checksum = checksum32(fw_map, fw_size);

		INFO("File Name: %s\n", fw_path);
		INFO("File Checksum: %x\n", fw_checksum);
	}

	current_fw_version = read_register(VERSION_REGISTER);
	if (current_fw_version == -1) {
		fprintf(stderr, "Unable to read current firwmware version\n");
		if (!emerg)
			return 1;
	} else {
		INFO("Device Firmware version: %d\n", current_fw_version);
	}

	if (version_check)
		return 0;

	if (enter_upgrade_mode()) {
		perror("Device didn't enter upgrade mode. \n");
		goto out;
	}

	if (check_ready_for_upgrade() && !emerg) {
		perror("Device Not ready for upgrade. \n");
		goto out;
	}

	if (validate_chip_id() && !emerg) {
		perror("Unexpected chip id.  Upgrade failed.\n");
		goto out;
	}

	if (interactive_mode) {
		printf("Ready to start update. Continue? [y/n] ");
		scanf("%c", &go);
		if (go != 'y' && go != 'Y') {
			LOG("User Abort\n");
			goto out;
		}
	}

	if (set_flash_length()) {
		perror("set flash length.\n");
		if (!emerg)
			goto out;
	}

	if (erase_flash()) {
		perror("Cannot erase flash.\n");
		if (!emerg)
			goto out;
	}
	sleep (2);

	if (write_firmware(fw_map, fw_size)) {
		perror("Cannot write firmware.");
		goto out;
	}

	sleep (2);

	if (get_device_checksum(&new_device_checksum)) {
		perror("Cannot fetch device firmware checksum.");
		goto out;
	}

	printf("Checksum on device: %x\n", new_device_checksum);

	if (new_device_checksum == fw_checksum) {
		ret = 0;
		INFO("Firmware update completed succesfully\n");
	} else
		INFO("Firmware update failed. \n");
out:
	if (exit_upgrade_mode())
		perror("Device didn't exit upgrade mode");


	return ret;
}
